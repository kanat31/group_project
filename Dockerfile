FROM debian:bookworm

ARG arg1=arg1
ENV arg1=${arg1}

ARG arg2=arg2
ENV arg2=${arg2}

ARG arg3=arg3
ENV arg3=${arg3}

ENV env1=env1
ENV env2=env2

COPY ./main.c ./

RUN apt update && apt -y install build-essential
RUN gcc -o main ./main.c
RUN ./main

ENTRYPOINT [""]

